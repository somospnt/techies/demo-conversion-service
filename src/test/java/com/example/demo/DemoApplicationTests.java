package com.example.demo;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import static org.assertj.core.api.Assertions.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.convert.ConversionService;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

    @Autowired
    private ConversionService conversionService;

    @Test
    public void convert_Strings_retornaLista() {
        List<String> list = conversionService.convert("Deb, Mike, Kim", List.class);
        assertThat(list.size()).isEqualTo(3);
    }
    
    @Test
    public void convert_String_retornaBigDecimal() {
        BigDecimal monto = conversionService.convert("10.333", BigDecimal.class);
        assertThat(monto.toString()).isEqualTo("10.333");
    }

    @Test
    public void convert_String_retornaCurrency() {
        Currency currency = conversionService.convert("ARS", Currency.class);
        assertThat(currency.getDisplayName()).isEqualTo("peso argentino");
    }

    @Test
    public void convert_String_retornaOptional() {
        Optional<String> optional = conversionService.convert("Algo", Optional.class);
        assertThat(optional.isPresent()).isTrue();
    }

    @Test
    public void convert_Date_retornaLocalDateTime() {
        LocalDateTime localeDate = conversionService.convert(new Date(), LocalDateTime.class);
        assertThat(localeDate).isInstanceOf(LocalDateTime.class);
    }

}
